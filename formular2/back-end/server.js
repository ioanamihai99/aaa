const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

//Initializing server
const app = express();
app.use(bodyParser.json());
const port = 8080;
app.listen(port, () => {
    console.log("Server online on: " + port);
});

app.use('/' , express.static('../front-end'))

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "siscit_abonament1"
});

connection.connect(function (err) {

    console.log("Connected to database!");
    const sql = "CREATE TABLE IF NOT EXISTS abonament(nume VARCHAR(40),prenume VARCHAR(40),telefon VARCHAR(15),email VARCHAR(50),facebook VARCHAR(60),tipAbonament VARCHAR(20),nrCard VARCHAR(40),cvv INTEGER, varsta VARCHAR(3), cnp VARCHAR(20))";
    connection.query(sql, function (err, result) {
        if (err) throw err;
    });
});

app.post("/abonament", (req, res) => {
    let nume = req.body.nume;
    let prenume = req.body.prenume;
    let telefon = req.body.telefon;
    let email = req.body.email;
    let facebook = req.body.facebook;
    let tipAbonament = req.body.tipAbonament;
    let nrCard = req.body.nrCard;
    let cvv = req.body.cvv;
    let varsta  = req.body.varsta;
    let cnp = req.body.cnp;
    let error = []
    if (!nume||!prenume||!telefon||!email||!facebook||!tipAbonament||!nrCard||!cvv||!varsta||!cnp) {
        error.push ("Unul sau mai multe campuri nu au fost introduse");
        console.log("Unul sau mai multe campuri nu au fost introduse!");
   } else
    {if (nume.length > 3 || nume.length < 20)
   {console.log("Nume invalid!");
   error.push("Nume invalid");}
   else if (!nume.match("^[A-Za-z]+$")){
       console.log("Numele introdus trebuie sa contina doar litere!");
       error.push("Numele trebuie sa contina doar litere!");
   }if (prenume.lenght >3 || prenume.lenght < 20)
   {console.log("Nume invalid!");
   error.push("Nume invalid!");}
   else if (!prenume.match("^[A-Za-z]+$")){
       console.log("Prenumele introdus trebuie sa contina doar litere!");
       error.push("Prenumele introdus trebuie sa contina doar litere!");
   }if (telefon.lenght != 10) {
       console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
       error.push("Numaarul trebuie sa fie de 10 cifre!");
   }else if(!telefon.match("^[0-9]+$")){
       console.log("Numarul de telefon trebuie sa contina doar cifre!");
       error.push("Numarul de telefon trebuie sa contina doar cifre!");
   }if (!email.includes("@gmail.com") || !email.includes("@yahoo.com") ){
       console.log("Email invalid!");
       error.push("Email invalid!");
   }if (nrCard != 16){
       console.log("Numarul de card trebuie sa fie de 16 cifre!");
       error.push("Numarul de card trebuie sa fie de 16 cifre");
   }else if(!nrCard.match("^[0-9]+$")){
       console.log("Numarul de card trebuie sa contina doar cifre!");
       error.push("Numarul de card trebuie sa contina doar cifre!");
   }if (cvv != 3){
       console.log("Cvv trebuie sa fie de 3 cifre!");
       error.push("Cvv trebuie sa fie de 3 cifre!");
   }else if(!cvv.match("^[0-9]+$")){
       console.log("Cvv trebuie sa contina doar cifre!");
       error.log("Cvv trebuie sa contina doar cifre!");
   }if (varsta.lenght > 1 || varsta.lenght < 4){
       console.log("Varsta invalida!");
       error.push("Varsta invalida!");
   }else if(!varsta.match("^[0-9]+$")){
       console.log("Varsta trebuie sa contina doar cifre!");
       error.push("Varsta trebuie sa contina doar cifre!");
   }if(!cnp.match("^[0-9]+$")){
       console.log("Cnp trebuie sa contina doar cifre!");
       error.log("Cnp trebuie sa contina doar cifre!");
   }else if("cnp != 13"){
       console.log("Cnp trebuie sa fie de 13 cifre!");
       error.push("Cnp trebuie sa fie de 13 cifre");
   }}

    if (error.length === 0) {
        const sql =
            "INSERT INTO abonament (nume,prenume,telefon,email,facebook,tipAbonament,nrCard,cvv) VALUES('" +nume +"','" +prenume +"','" +telefon +"','" +email +"','" +facebook +"','" + tipAbonament +"','" + nrCard +"','" +cvv+"')";
        connection.query(sql, function (err, result) {
            if (err) throw err;
            console.log("Abonament achizitionat!");
        });

        res.status(200).send({
            message: "Abonament achizitionat!"
        });
        console.log(sql);
    } else {
        res.status(500).send(error)
        console.log("Eroare la inserarea in baza de date!");
    }
});